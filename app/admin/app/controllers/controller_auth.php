<?php
class Controller_Auth extends Controller
{

	function __construct()
	{
		$this->model = new Model_Auth();
		$this->view = new View();
	}
	
	function action_index()
	{
		//$data = $this->model->get_data();		
        $email = false;
        $password = false;
        //var_dump($_SESSION['user']);
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $errors = false;
            //$errors[] = 'Неправильный email';
                $check = $this->model->check_data($_POST['email']);
                $hashed_password = $check[0][2];
                //var_dump($hashed_password);
                $userId = $check[0][0];
                //var_dump($this->verify($password, $hashed_password));
                if ($this->verify($password, $hashed_password)) {
                    $this->auth($userId);
                    $this->view->generate('authmain_view.php', 'template_view.php', $data,'app/admin/');
                } else 
                {
                    $this->view->generate('auth_view.php', 'template_view.php', $data,'app/admin/');
                }
            
        }
        else
        {
            //require_once(ROOT . '/user/login.php');
            //return true;
            $this->view->generate('auth_view.php', 'template_view.php', $data,'app/admin/');
        }
	}
   function action_login()
   {
       
   }
   function action_logout()
   {
        unset($_SESSION["user"]);
        //session_start();
        header("Location: /");
        return true;
   }
	
    function verify($password, $hashedPassword) {
        //echo crypt($password, $hashedPassword);
    return crypt($password, $hashedPassword) == $hashedPassword;
    }
    public static function auth($userId)
    {
        // Записываем идентификатор пользователя в сессию
        
        $_SESSION['user'] = $userId;
    }
    public static function isGuest()
{
    if (isset($_SESSION['user'])) return false;
    else return true;
}
}
?>