<?php
class Controller_News extends Controller
{

	function __construct()
	{
		$this->model = new Model_News();
		$this->view = new View();
	}
	
	function action_index()
	{
		$data = $this->model->get_data();		
		$this->view->generate('news_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
	}
   function action_add($Title = NULL, $Description=NULL)
	{
		$data = $this->model->get_data();		
		
        //var_dump($_POST);
        //echo isset($_POST['title']) && isset($_POST['Description']);
        //$act = array();
        if (isset($_POST['title']) && isset($_POST['Description']))
        {
            $tags = explode(',', $_POST['id_of_tag']);
            $data = $this->model->add_data($_POST['title'],$_POST['Description']);
//////////////////////////////////////////////////////////////////////////////////////
            foreach($tags as &$value)
            {
                $this->model->add_tags_to_news($data,$value);
            }
/////////////////////////////////////////////////////////////////////////////////////
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
            header('Location:'.$host);
        }
        else
        {
            $this->view->generate('newsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
        }
	}
    function action_del()
	{
		$data = $this->model->del_data($_GET['id']);		
        //удаление тегов
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
        header('Location:'.$host);

	}
    
    function action_update()
	{
        if (isset($_POST['title']) && isset($_POST['Description']))
        {
            $data = $this->model->update_data($_POST['title'],$_POST['Description'], $_GET['id']);		
            //var_dump($_POST['id_of_tag']);
            $tags = explode(',', $_POST['id_of_tag']);
            //////////////////////////////////////////////////////////////////////////////////////
            foreach($tags as &$value)
            {
                //echo $value;
                $this->model->add_tags_to_news($_GET['id'],$value);
            }
/////////////////////////////////////////////////////////////////////////////////////
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/news/';
            header('Location:'.$host);
        }
        else
        {
            $this->view->generate('newsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
        }
	}

}
?>