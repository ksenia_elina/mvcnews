<?php
class Controller_tags extends Controller
{

	function __construct()
	{
		$this->model = new Model_Tags();
		$this->view = new View();
	}
	
	function action_index()
	{
		$data = $this->model->get_data();		
		$this->view->generate('tags_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
	}
    
       function action_add($Title = NULL, $Description=NULL)
	{

        if (isset($_POST['title']))
        {
            $data = $this->model->add_data($_POST['title']);		
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
            header('Location:'.$host);
        }
        else
        {
            $this->view->generate('tagsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
        }
	}
    function action_del()
	{
		$data = $this->model->del_data($_GET['id']);		
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
        header('Location:'.$host);

	}
    
    function action_update()
	{
        if (isset($_POST['title']))
        {
            $data = $this->model->update_data($_POST['title'], $_GET['id']);		
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/admin/tags/';
            header('Location:'.$host);
        }
        else
        {
            $this->view->generate('tagsedit_view.php', 'template_view.php', $data,'app/admin/', $this->IsGuest());
        }
	}
}
?>