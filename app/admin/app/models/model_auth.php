<?php
class Model_Auth extends Model
{
    public $dbh;

    public function get_data()
    {
        $sth = $this->dbh->prepare('SELECT id, title
                FROM tags ');
        //$sth->execute(array(150, 'red'));
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }
    
        public function add_data($title)
    {
        $stmt = $this->dbh->prepare("INSERT INTO `tags` (`id`, `title`) VALUES (NULL, ?);");
        mysqli_stmt_bind_param($stmt, 's', $title);

        $stmt->execute();
    }
    
        public function del_data($id)
    {
        $stmt = $this->dbh->prepare("DELETE FROM `tags` WHERE `tags`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 's', $id);

        $stmt->execute();
    }
    
        public function update_data($title, $id)
    {
        $stmt = $this->dbh->prepare("UPDATE `tags` SET `title` = ? WHERE `tags`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 'ss', $title, $id);

        $stmt->execute();
        //echo $stmt;
    }
    
    public function check_data($email)
    {


    $stmt = $this->dbh->prepare("SELECT * FROM `users` WHERE `users`.`email` = ?;");
    mysqli_stmt_bind_param($stmt, 's', $email);
    $stmt->execute();
    $res = $stmt->get_result();
    $result = $res->fetch_all();
        //var_dump($result);
    return $result;

    }
}
?>