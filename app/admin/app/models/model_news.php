<?php
class Model_News extends Model
{
    public $dbh;
	public function get_data2()
	{	
		return array(
			
			array(
				'Title' => '2012',
				//'Site' => 'http://DunkelBeer.ru',
				'Description' => 'Промо-сайт темного пива Dunkel от немецкого производителя Löwenbraü выпускаемого в России пивоваренной компанией "CАН ИнБев".'
			),
			array(
				'Title' => '2012',
				//'Site' => 'http://ZopoMobile.ru',
				'Description' => 'Русскоязычный каталог китайских телефонов компании Zopo на базе Android OS и аксессуаров к ним.'
			),
			// todo
		);
	}
    public function get_data()
    {
        $sth = $this->dbh->prepare('SELECT id, title, Description
                FROM newslist ');
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }
    
    public function add_data($title, $description)
    {
        $stmt = $this->dbh->prepare("INSERT INTO `newslist` (`id`, `title`, `description`) VALUES (NULL, ?,?);");
        mysqli_stmt_bind_param($stmt, 'ss', $title, $description);

        $stmt->execute();
        //var_dump($stmt);
        return $stmt->insert_id;
    }
    
        public function del_data($id)
    {
        $stmt = $this->dbh->prepare("DELETE FROM `newslist` WHERE `newslist`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 's', $id);

        $stmt->execute();
        $this->del_tags_from_news($id);
    }
    
        public function update_data($title, $description, $id)
    {
        $stmt = $this->dbh->prepare("UPDATE `newslist` SET `title` = ?,`description` = ? WHERE `newslist`.`id` = ?;");
        mysqli_stmt_bind_param($stmt, 'sss', $title, $description, $id);

        $stmt->execute();
        $this->del_tags_from_news($id);
        //echo $stmt;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////связанная таблица    
    public function add_tags_to_news($idnews, $idtag)
    {
        $stmt = $this->dbh->prepare("INSERT INTO `tags_news` (`id`, `id_of_news`, `id_of_tag`) VALUES (NULL, ?,?);");
        mysqli_stmt_bind_param($stmt, 'ss', $idnews, $idtag);

        $stmt->execute();
        //var_dump($idnews);
    }
    public function del_tags_from_news($idnews)
    {
        $stmt = $this->dbh->prepare("DELETE FROM `tags_news` WHERE `tags_news`.`id_of_news` = ?;");
        mysqli_stmt_bind_param($stmt, 's', $idnews);

        $stmt->execute();
    }
}
?>