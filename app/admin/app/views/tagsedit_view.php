<form method="post" >
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Enter title">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>