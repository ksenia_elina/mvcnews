<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Главная</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
<div class="navbar navbar-expand-lg navbar-light bg-light">
    <a href="./news" class="list-group-item active">
        <span class="glyphicon glyphicon-star"></span> Новости 
    </a>
    <a href="./tags" class="list-group-item">
        <span class="glyphicon glyphicon-user"></span> Теги 
    </a>
    <a href="./auth/logout" class="list-group-item">
        <span class="glyphicon glyphicon-th-list"></span> Выйти
    </a>
</div>
	<?php 
    if ((isset($Guest))&&!$Guest)
    {

        include $admin_dir.'app/views/'.$content_view;
    }?>
</body>
</html>