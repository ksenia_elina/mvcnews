<?php
class Controller_Newstags extends Controller
{

	function __construct()
	{
		$this->model = new Model_Newstags();
		$this->view = new View();
	}
	
	function action_index()
	{
		$data = $this->model->get_datainf($_GET['tag']);		
        //echo "ok";
		$this->view->generate('newstags_view.php', 'template_view.php', $data);
	}
}
?>