<?php
class Model_Newstags extends Model
{
    public $dbh;
    public function get_datainf($tag)
    {
        $sth = $this->dbh->prepare('SELECT n.id, n.title, n.description,  t.title  FROM `newslist` as n Left OUTER JOIN `tags_news` as tn ON n.id = tn.id_of_news Left OUTER  JOIN `tags` as t ON tn.id_of_tag = t.id where t.title=?;');
        //$sth->execute(array(150, 'red'));
        mysqli_stmt_bind_param($sth, 's', $tag);
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }
        public function get_datainf_limit($tag, $start, $num)
    {
        $sth = $this->dbh->prepare('SELECT n.id, n.title, n.description,  t.title  FROM `newslist` as n Left OUTER JOIN `tags_news` as tn ON n.id = tn.id_of_news Left OUTER  JOIN `tags` as t ON tn.id_of_tag = t.id where t.title=? LIMIT ?,?;');
        //$sth->execute(array(150, 'red'));
        mysqli_stmt_bind_param($sth, 's', $tag, $start, $num);
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }

}
?>
