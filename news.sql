-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 10 2020 г., 12:22
-- Версия сервера: 5.6.41
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `news`
--

-- --------------------------------------------------------

--
-- Структура таблицы `newslist`
--

CREATE TABLE `newslist` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `newslist`
--

INSERT INTO `newslist` (`id`, `title`, `description`) VALUES
(1, 'Первая статья', 'Первая статья описание'),
(2, 'Вторая статья', 'Вторая статья описание'),
(3, 'Третья статья', 'Третья статья описание'),
(4, 'Четвертая статья', 'Четвертая статья описание'),
(5, '1', '1'),
(6, '1', '1'),
(8, 'Category #1', '123'),
(9, 'Category #1', '123'),
(10, 'Category #1', '123'),
(11, 'Category #1', '123'),
(12, 'Category #1555', '123'),
(17, 'Category #1555i', '123'),
(18, 'Category #1', '123'),
(19, 'Category #1', '123'),
(20, 'Category #1', '123'),
(21, 'Category #1', '123'),
(22, 'Category #1', '123'),
(23, 'Category #1', '123'),
(31, 'Category #1', '123'),
(33, 'Category #31', '123'),
(35, '111', '123');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `title`) VALUES
(1, 'квартира'),
(2, 'машина'),
(6, 'Category #31'),
(7, 'Category #1555i');

-- --------------------------------------------------------

--
-- Структура таблицы `tags_news`
--

CREATE TABLE `tags_news` (
  `id` int(11) NOT NULL,
  `id_of_news` int(10) NOT NULL,
  `id_of_tag` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tags_news`
--

INSERT INTO `tags_news` (`id`, `id_of_news`, `id_of_tag`) VALUES
(1, 30, 0),
(2, 31, 1),
(13, 0, 1),
(14, 0, 2),
(15, 0, 1),
(16, 0, 2),
(17, 0, 1),
(18, 0, 2),
(24, 35, 1),
(25, 35, 2),
(26, 35, 3),
(27, 35, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'oksforspam@yandex.ru', '20EAfcH0JSFQY');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `newslist`
--
ALTER TABLE `newslist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags_news`
--
ALTER TABLE `tags_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `newslist`
--
ALTER TABLE `newslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `tags_news`
--
ALTER TABLE `tags_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
